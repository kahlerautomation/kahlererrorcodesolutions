﻿// <auto-generated />
namespace KahlerErrorCodes.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class AddApplicationTypes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddApplicationTypes));
        
        string IMigrationMetadata.Id
        {
            get { return "202005052137374_AddApplicationTypes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
