﻿namespace KahlerErrorCodes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedApplicationTypeName2 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.ErrorCodeSolution", "solution_id");
            CreateIndex("dbo.ErrorCodeSolution", "error_code_id");
            CreateIndex("dbo.SoftwareApplication", "application_type_id");
            AddForeignKey("dbo.ErrorCodeSolution", "error_code_id", "dbo.ErrorCode", "id", cascadeDelete: true);
            AddForeignKey("dbo.ErrorCodeSolution", "solution_id", "dbo.Solution", "id", cascadeDelete: true);
            AddForeignKey("dbo.SoftwareApplication", "application_type_id", "dbo.ApplicationType", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SoftwareApplication", "application_type_id", "dbo.ApplicationType");
            DropForeignKey("dbo.ErrorCodeSolution", "solution_id", "dbo.Solution");
            DropForeignKey("dbo.ErrorCodeSolution", "error_code_id", "dbo.ErrorCode");
            DropIndex("dbo.SoftwareApplication", new[] { "application_type_id" });
            DropIndex("dbo.ErrorCodeSolution", new[] { "error_code_id" });
            DropIndex("dbo.ErrorCodeSolution", new[] { "solution_id" });
        }
    }
}
