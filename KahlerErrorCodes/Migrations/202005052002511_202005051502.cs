﻿namespace KahlerErrorCodes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveErrorCodeSolutionForeignKeys : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ErrorCodeSolution", "error_code_id", "dbo.ErrorCode");
            DropForeignKey("dbo.ErrorCodeSolution", "solution_id", "dbo.Solution");
            DropIndex("dbo.ErrorCodeSolution", new[] { "solution_id" });
            DropIndex("dbo.ErrorCodeSolution", new[] { "error_code_id" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.ErrorCodeSolution", "error_code_id");
            CreateIndex("dbo.ErrorCodeSolution", "solution_id");
            AddForeignKey("dbo.ErrorCodeSolution", "solution_id", "dbo.Solution", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ErrorCodeSolution", "error_code_id", "dbo.ErrorCode", "ID", cascadeDelete: true);
        }
    }
}
