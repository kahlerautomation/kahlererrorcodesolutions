﻿namespace KahlerErrorCodes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddApplicationTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationType",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        application_identifier = c.String(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ErrorCodeSolution", "application_type_id", c => c.Guid(nullable: false));
            AddColumn("dbo.ErrorCodeSolution", "application", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ErrorCodeSolution", "application");
            DropColumn("dbo.ErrorCodeSolution", "application_type_id");
            DropTable("dbo.ApplicationType");
        }
    }
}
