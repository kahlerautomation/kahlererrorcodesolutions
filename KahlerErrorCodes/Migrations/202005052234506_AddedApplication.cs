﻿namespace KahlerErrorCodes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedApplication : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SoftwareApplication",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        application_identifier = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        application_type_id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ErrorCodeSolution", "software_application_id", c => c.String());
            DropColumn("dbo.ErrorCodeSolution", "application");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ErrorCodeSolution", "application", c => c.String());
            DropColumn("dbo.ErrorCodeSolution", "software_application_id");
            DropTable("dbo.SoftwareApplication");
        }
    }
}
