﻿namespace KahlerErrorCodes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKeysForApplication : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ErrorCodeSolution", "application_type_id", c => c.Guid());
            AlterColumn("dbo.ErrorCodeSolution", "software_application_id", c => c.Guid());
            CreateIndex("dbo.ErrorCodeSolution", "application_type_id");
            CreateIndex("dbo.ErrorCodeSolution", "software_application_id");
            AddForeignKey("dbo.ErrorCodeSolution", "application_type_id", "dbo.ApplicationType", "id");
            AddForeignKey("dbo.ErrorCodeSolution", "software_application_id", "dbo.SoftwareApplication", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ErrorCodeSolution", "software_application_id", "dbo.SoftwareApplication");
            DropForeignKey("dbo.ErrorCodeSolution", "application_type_id", "dbo.ApplicationType");
            DropIndex("dbo.ErrorCodeSolution", new[] { "software_application_id" });
            DropIndex("dbo.ErrorCodeSolution", new[] { "application_type_id" });
            AlterColumn("dbo.ErrorCodeSolution", "software_application_id", c => c.String());
            AlterColumn("dbo.ErrorCodeSolution", "application_type_id", c => c.Guid(nullable: false));
        }
    }
}
