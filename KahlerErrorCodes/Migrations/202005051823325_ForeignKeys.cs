﻿namespace KahlerErrorCodes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKeys : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ErrorCode",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        error_code = c.String(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ErrorCodeSolution",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        solution_id = c.Guid(nullable: false),
                        error_code_id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ErrorCode", t => t.error_code_id, cascadeDelete: true)
                .ForeignKey("dbo.Solution", t => t.solution_id, cascadeDelete: true)
                .Index(t => t.solution_id)
                .Index(t => t.error_code_id);
            
            CreateTable(
                "dbo.Solution",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ErrorCodeSolution", "solution_id", "dbo.Solution");
            DropForeignKey("dbo.ErrorCodeSolution", "error_code_id", "dbo.ErrorCode");
            DropIndex("dbo.ErrorCodeSolution", new[] { "error_code_id" });
            DropIndex("dbo.ErrorCodeSolution", new[] { "solution_id" });
            DropTable("dbo.Solution");
            DropTable("dbo.ErrorCodeSolution");
            DropTable("dbo.ErrorCode");
        }
    }
}
