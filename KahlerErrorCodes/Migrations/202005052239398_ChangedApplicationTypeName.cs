﻿namespace KahlerErrorCodes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedApplicationTypeName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ApplicationType", "Name", c => c.String(nullable: false));
            DropColumn("dbo.ApplicationType", "application_identifier");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ApplicationType", "application_identifier", c => c.String(nullable: false));
            DropColumn("dbo.ApplicationType", "Name");
        }
    }
}
