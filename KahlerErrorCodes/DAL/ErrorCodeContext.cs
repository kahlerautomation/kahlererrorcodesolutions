﻿using KahlerErrorCodes.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace KahlerErrorCodes.DAL {
    public class ErrorCodeContext : DbContext {

        public ErrorCodeContext() : base("ErrorCodeContext") {
        }

        public DbSet<ErrorCode> ErrorCodes { get; set; }
        public DbSet<Solution> Solutions { get; set; }
        public DbSet<ErrorCodeSolution> ErrorCodeSolutions { get; set; }
        public DbSet<ApplicationType> ApplicationTypes { get; set; }
        public DbSet<SoftwareApplication> SoftwareApplications { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<ErrorCodeSolution>()
              .HasOptional(c => c.ApplicationType);
            modelBuilder.Entity<ErrorCodeSolution>()
              .HasOptional(c => c.SoftwareApplication);

        }


    }
}