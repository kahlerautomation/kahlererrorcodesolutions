﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace KahlerErrorCodes.Models {
    public class ApplicationType {
        [Key, Required, Column("id")]
        public Guid ApplicationTypeId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public virtual IEnumerable<Solution> Solutions { get; set; }

        public virtual IEnumerable<SoftwareApplication> Applications { get; set; }
    }
}