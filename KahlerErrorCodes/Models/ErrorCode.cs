﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace KahlerErrorCodes.Models {
    public class ErrorCode {
        [Required, Key, Column("id")]
        public Guid ErrorCodeId { get; set; }

        [Required, Column("error_code")]
        public string Code { get; set; }

        [Required]
        public string Description { get; set; }

        public virtual IEnumerable<Solution> Solutions { get; set; }

        public string DisplayableName {
            get {
                return $"{Code} - {Description}";
            }
        }
    }
}