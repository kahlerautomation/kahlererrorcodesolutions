﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KahlerErrorCodes.Models {
    public class Solution {
        [Key, Required, Column("id")]
        public Guid SolutionId { get; set; }
        [Required]
        public string Description { get; set; }

        public virtual IEnumerable<ErrorCodeSolution> ErrorCodeSolutions { get; set; }
    }
}