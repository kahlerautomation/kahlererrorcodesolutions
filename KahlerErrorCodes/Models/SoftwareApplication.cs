﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KahlerErrorCodes.Models {
    public class SoftwareApplication {
        [Key, Required, Column("id")]
        public Guid SoftwareApplicationId { get; set; }

        [Required, Column("application_identifier"), Display(Name = "Application identifier")]
        public string ApplicationIdentifier { get; set; }

        [Required]
        public string Name { get; set; }

        [Required, Column("application_type_id"), Display(Name = "Application type")]
        public Guid ApplicationTypeId { get; set; }
        public virtual ApplicationType ApplicationType { get; set; }
        public string ApplicationTypeName { get { return ApplicationType?.Name ?? ""; } }
    }
}