﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KahlerErrorCodes.Models {
    public class ErrorCodeSolution {
        [Key, Required]
        public Guid Id { get; set; }

        [Required, Column("solution_id"), Display(Name = "Solution")]
        public Guid SolutionId { get; set; }
        public virtual Solution Solution { get; set; }

        [Required, Column("error_code_id"), Display(Name = "Error code")]
        public Guid ErrorCodeId { get; set; }
        public virtual ErrorCode ErrorCode { get; set; }

        [Column("application_type_id"), Display(Name = "Application type")]
        public Guid? ApplicationTypeId { get; set; }
        public virtual ApplicationType ApplicationType { get; set; }
        public string ApplicationTypeName { get { return ApplicationType?.Name ?? ""; } }

        [Column("software_application_id"), Display(Name = "Application")]
        public Guid? SoftwareApplicationId { get; set; }
        public virtual SoftwareApplication SoftwareApplication { get; set; }
        public string SoftwareApplicationName { get { return SoftwareApplication?.Name ?? ""; } }
    }
}