﻿using System.Collections.Generic;

namespace KahlerErrorCodes.Models {
    public class ErrorCodeSolutionResponse {
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
        public List<string> SoftwareSpecificSolutionsForError { get; set; }
        public List<string> ProductTypeSolutionsForError { get; set; }
        public List<string> GenericSolutionsForError { get; set; }
    }
}