﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using KahlerErrorCodes.DAL;
using KahlerErrorCodes.Models;

namespace KahlerErrorCodes.Controllers {
    public class SolutionController : Controller {
        private ErrorCodeContext db = new ErrorCodeContext();

        public ActionResult Index() {
            return View(db.Solutions
                .ToList()
                .OrderBy((s) => s.Description));
        }

        public ActionResult Details(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Solution solution = db.Solutions.Find(id);
            var errorCodeSolutions = db.ErrorCodeSolutions.Where(ecs => ecs.SolutionId == id.Value).ToList();
            solution.ErrorCodeSolutions = errorCodeSolutions;
            return View(solution);
        }

        public ActionResult Create() {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SolutionId,Description")] Solution solution) {
            if (ModelState.IsValid) {
                solution.SolutionId = Guid.NewGuid();

                db.Solutions.Add(solution);
                db.SaveChanges();
            }

            return View(solution);
        }

        public ActionResult Edit(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Solution solution = db.Solutions.Find(id);
            if (solution == null) {
                return HttpNotFound();
            }
            solution.ErrorCodeSolutions = db.ErrorCodeSolutions.Where(ecs => ecs.SolutionId == id.Value).ToList();
            return View(solution);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SolutionId,Description")] Solution solution) {
            if (ModelState.IsValid) {
                db.Entry(solution).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(solution);
        }

        public ActionResult Delete(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Solution solution = db.Solutions.Find(id);
            if (solution == null) {
                return HttpNotFound();
            }
            return View(solution);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id) {
            Solution solution = db.Solutions.Find(id);
            db.Solutions.Remove(solution);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
