﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using KahlerErrorCodes.DAL;
using KahlerErrorCodes.Models;

namespace KahlerErrorCodes.Controllers {
    public class ErrorCodeController : Controller {
        private ErrorCodeContext db = new ErrorCodeContext();

        public ActionResult Index() {
            return View(db.ErrorCodes
                .ToList()
                .OrderBy((e) => e.Code));
        }

        //public ActionResult Index(Guid? solutionID, Guid? errorCodeID) {
        //    var viewModel = new ErrorCodeSolutionData();

        //    if (solutionID != null) {
        //        viewModel.Solutions = db.Solutions
        //            .ToList()
        //            .OrderBy((e) => e.Description);
        //        ViewBag.SolutionID = solutionID.Value;
        //        viewModel.ErrorCodes = db.ErrorCodes.Where(e => db.ErrorCodeSolutions.Where(ecs => ecs.SolutionId == solutionID.Value).Select(ecs => ecs.ErrorCodeId).ToList().Contains(e.ID)).ToList();
        //    } else if (errorCodeID != null) {
        //        viewModel.ErrorCodes = db.ErrorCodes
        //           .ToList()
        //           .OrderBy((e) => e.DisplayableName());
        //        ViewBag.ErrorCodeID = errorCodeID.Value;
        //        viewModel.Solutions = db.Solutions.Where(s => db.ErrorCodeSolutions.Where(ecs => ecs.ErrorCodeId == errorCodeID.Value).Select(ecs => ecs.SolutionId).ToList().Contains(s.ID)).ToList();
        //    } else {
        //        viewModel.ErrorCodes = db.ErrorCodes
        //            .ToList()
        //            .OrderBy((e) => e.DisplayableName());
        //        viewModel.Solutions = db.Solutions
        //            .ToList()
        //            .OrderBy((e) => e.Description);

        //    }

        //    return View(viewModel);
        //}

        public ActionResult Details(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ErrorCode errorCode = db.ErrorCodes.Find(id);
            errorCode.Solutions = db.Solutions.Where(s => db.ErrorCodeSolutions.Where(ecs => ecs.ErrorCodeId == errorCode.ErrorCodeId).Select(ecs => ecs.SolutionId).ToList().Contains(s.SolutionId)).ToList();

            return View(errorCode);
        }

        public ActionResult Create() {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ErrorCodeId,Code,Description")] ErrorCode errorCode) {
            try {
                if (ModelState.IsValid) {
                    errorCode.ErrorCodeId = Guid.NewGuid();
                    db.ErrorCodes.Add(errorCode);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            } catch (DataException /* dex */) {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            return View(errorCode);
        }

        public ActionResult Edit(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ErrorCode errorCode = db.ErrorCodes.Find(id);
            if (errorCode == null) {
                return HttpNotFound();
            }
            errorCode.Solutions = db.Solutions.Where(s => db.ErrorCodeSolutions.Where(ecs => ecs.ErrorCodeId == errorCode.ErrorCodeId).Select(ecs => ecs.SolutionId).ToList().Contains(s.SolutionId)).ToList();
            return View(errorCode);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ErrorCodeId,Code,Description")] ErrorCode errorCode) {
            try {
                if (ModelState.IsValid) {
                    db.Entry(errorCode).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            } catch (DataException /* dex */) {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(errorCode);
        }

        public ActionResult Delete(Guid? id, bool? saveChangesError = false) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault()) {
                ViewBag.ErrorMessage = "Delete failed. Try again, and if the problem persists see your system administrator.";
            }
            ErrorCode errorCode = db.ErrorCodes.Find(id);
            if (errorCode == null) {
                return HttpNotFound();
            }
            return View(errorCode);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id) {
            try {
                ErrorCode errorCode = db.ErrorCodes.Find(id);
                db.ErrorCodes.Remove(errorCode);
                db.SaveChanges();
            } catch (DataException/* dex */) {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        public ActionResult GetSolutions(string id, string productType, string productCode) {
            return View(ErrorCodeSolutionController.GetSolutionsForErrorCode(db, id, productType, productCode));
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
