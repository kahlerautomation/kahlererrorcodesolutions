﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KahlerErrorCodes.DAL;
using KahlerErrorCodes.Models;

namespace KahlerErrorCodes.Controllers {
    public class SoftwareApplicationController : Controller {
        private ErrorCodeContext db = new ErrorCodeContext();

        public ActionResult Index() {
            return View(db.SoftwareApplications.ToList());
        }

        public ActionResult Details(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SoftwareApplication softwareApplication = db.SoftwareApplications.Find(id);
            if (softwareApplication == null) {
                return HttpNotFound();
            }
            return View(softwareApplication);
        }

        public ActionResult Create() {
            PopulateApplicationTypesDropDownList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SoftwareApplicationId,ApplicationIdentifier,Name,ApplicationTypeId")] SoftwareApplication softwareApplication) {
            try {
                if (ModelState.IsValid) {
                    softwareApplication.SoftwareApplicationId = Guid.NewGuid();
                    db.SoftwareApplications.Add(softwareApplication);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            } catch (RetryLimitExceededException /* dex */) {
                //Log the error (uncomment dex variable name and add a line here to write a log.)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }
            PopulateApplicationTypesDropDownList(softwareApplication.ApplicationTypeId);

            return View(softwareApplication);
        }

        public ActionResult Edit(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var softwareApplication = db.SoftwareApplications.Find(id);
            try {
                PopulateApplicationTypesDropDownList(softwareApplication.ApplicationTypeId);
            } catch (RetryLimitExceededException /* dex */) {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }
            return View(softwareApplication);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SoftwareApplicationId,ApplicationIdentifier,Name,ApplicationTypeId")] SoftwareApplication softwareApplication) {
            if (ModelState.IsValid) {
                db.Entry(softwareApplication).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            try {
                PopulateApplicationTypesDropDownList(softwareApplication.ApplicationTypeId);
            } catch (RetryLimitExceededException /* dex */) {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }
            return View(softwareApplication);
        }

        public ActionResult Delete(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SoftwareApplication softwareApplication = db.SoftwareApplications.Find(id);
            if (softwareApplication == null) {
                return HttpNotFound();
            }
            return View(softwareApplication);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id) {
            SoftwareApplication softwareApplication = db.SoftwareApplications.Find(id);
            db.SoftwareApplications.Remove(softwareApplication);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void PopulateApplicationTypesDropDownList(object selectedApplicationType = null) {
            ViewBag.ApplicationTypeID = new SelectList(db.ApplicationTypes.OrderBy(at => at.Name), "ApplicationTypeId", "Name", selectedApplicationType);
        }
    }
}
