﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using KahlerErrorCodes.DAL;
using KahlerErrorCodes.Models;

namespace KahlerErrorCodes.Controllers {
    public class ApplicationTypeController : Controller {
        private ErrorCodeContext db = new ErrorCodeContext();

        public ActionResult Index() {
            return View(db.ApplicationTypes.ToList());
        }

        public ActionResult Details(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationType applicationType = db.ApplicationTypes.Find(id);
            if (applicationType == null) {
                return HttpNotFound();
            }

            applicationType.Solutions = db.Solutions.Where(s => db.ErrorCodeSolutions.Where(ecs => ecs.ApplicationTypeId == applicationType.ApplicationTypeId).Select(ecs => ecs.SolutionId).ToList().Contains(s.SolutionId)).ToList();

            applicationType.Applications = db.SoftwareApplications.Where(s => s.ApplicationTypeId.Equals(applicationType.ApplicationTypeId)).ToList();

            return View(applicationType);
        }
         
        public ActionResult Create() {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApplicationTypeId,Name,Description")] ApplicationType applicationType) {
            if (ModelState.IsValid) {
                applicationType.ApplicationTypeId = Guid.NewGuid();
                db.ApplicationTypes.Add(applicationType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(applicationType);
        }

        public ActionResult Edit(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationType applicationType = db.ApplicationTypes.Find(id);
            if (applicationType == null) {
                return HttpNotFound();
            }
            return View(applicationType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ApplicationTypeId,Name,Description")] ApplicationType applicationType) {
            if (ModelState.IsValid) {
                db.Entry(applicationType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(applicationType);
        }

        public ActionResult Delete(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationType applicationType = db.ApplicationTypes.Find(id);
            if (applicationType == null) {
                return HttpNotFound();
            }
            if (db.Solutions.Where(s => db.ErrorCodeSolutions.Where(ecs => ecs.ApplicationTypeId == applicationType.ApplicationTypeId).Select(ecs => ecs.SolutionId).ToList().Contains(s.SolutionId)).Count() > 0) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (db.SoftwareApplications.Where(s => s.ApplicationTypeId.Equals(applicationType.ApplicationTypeId)).Count() > 0) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(applicationType);
        }
         
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id) {
            ApplicationType applicationType = db.ApplicationTypes.Find(id);
            db.ApplicationTypes.Remove(applicationType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
