﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KahlerErrorCodes.DAL;
using KahlerErrorCodes.Models;

namespace KahlerErrorCodes.Controllers {
    public class ErrorCodeSolutionController : Controller {
        private ErrorCodeContext db = new ErrorCodeContext();

        [HttpGet]
        public ActionResult GetSolutions(string id, string productType, string productCode) {
            ErrorCodeSolutionResponse solution = GetSolutionsForErrorCode(db, id, productType, productCode);

            if (Request.ContentType.Equals("text/html")) {
                string htmlReturn = $"<div><dl><dt>Error</dt><dd>{solution.ErrorCode} - {solution.ErrorDescription}</dd><dt>Solutions</dt><dd>";
                if (solution.GenericSolutionsForError.Count > 0 || solution.ProductTypeSolutionsForError.Count > 0 || solution.SoftwareSpecificSolutionsForError.Count > 0) {
                    htmlReturn += $"<ul><li>{string.Join("</li><li>", solution.SoftwareSpecificSolutionsForError)}{string.Join("</li><li>", solution.ProductTypeSolutionsForError)}{string.Join("</li><li>", solution.GenericSolutionsForError)}</li></ul>";
                } else {
                    htmlReturn += "<p>No solutions defined</p>";
                }
                htmlReturn += "</dd></dl></div>";
                return Content(htmlReturn);
            } else if (Request.ContentType.Equals("text/xml") || Request.ContentType.Equals("application/xml")) {
                return Content(KaCommonObjects.XmlMethods.ToXml(solution));
            } else { // if (Request.ContentType.Equals("application/json")) {
                return Json(solution, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Index() {
            var errorCodeSolutions = db.ErrorCodeSolutions.Include(e => e.ErrorCode).Include(e => e.Solution);
            return View(errorCodeSolutions.ToList());
        }

        public ActionResult Details(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ErrorCodeSolution errorCodeSolution = db.ErrorCodeSolutions.Find(id);
            if (errorCodeSolution == null) {
                return HttpNotFound();
            }
            return View(errorCodeSolution);
        }

        public ActionResult Create(Guid solutionId) {
            ErrorCodeSolution errorCodeSolution = new ErrorCodeSolution() { SolutionId = solutionId };
            PopulateSolutionsDropDownList(errorCodeSolution.SolutionId);
            PopulateErrorCodesDropDownList(errorCodeSolution.ErrorCodeId);
            PopulateApplicationTypesDropDownList(errorCodeSolution.ApplicationTypeId);
            PopulateSoftwareApplicationsDropDownList(errorCodeSolution.SoftwareApplicationId);
            return View(errorCodeSolution);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SolutionId,ErrorCodeId,ApplicationTypeId,SoftwareApplicationId")] ErrorCodeSolution errorCodeSolution) {
            if (ModelState.IsValid) {
                errorCodeSolution.Id = Guid.NewGuid();
                db.ErrorCodeSolutions.Add(errorCodeSolution);
                db.SaveChanges();
                return RedirectToAction("Edit", "Solution", new { id = errorCodeSolution.SolutionId });
            }

            PopulateSolutionsDropDownList(errorCodeSolution.SolutionId);
            PopulateErrorCodesDropDownList(errorCodeSolution.ErrorCodeId);
            PopulateApplicationTypesDropDownList(errorCodeSolution.ApplicationTypeId);
            PopulateSoftwareApplicationsDropDownList(errorCodeSolution.SoftwareApplicationId);

            return View(errorCodeSolution);
        }

        public ActionResult Edit(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ErrorCodeSolution errorCodeSolution = db.ErrorCodeSolutions.Find(id);
            if (errorCodeSolution == null) {
                return HttpNotFound();
            }

            try {
                PopulateSolutionsDropDownList(errorCodeSolution.SolutionId);
                PopulateErrorCodesDropDownList(errorCodeSolution.ErrorCodeId);
                PopulateApplicationTypesDropDownList(errorCodeSolution.ApplicationTypeId);
                PopulateSoftwareApplicationsDropDownList(errorCodeSolution.SoftwareApplicationId);
            } catch (RetryLimitExceededException /* dex */) {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }
            return View(errorCodeSolution);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SolutionId,ErrorCodeId,ApplicationTypeId,SoftwareApplicationId")] ErrorCodeSolution errorCodeSolution) {
            if (ModelState.IsValid) {
                db.Entry(errorCodeSolution).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", "Solution", new { id = errorCodeSolution.SolutionId });
            }
            try {
                PopulateSolutionsDropDownList(errorCodeSolution.SolutionId);
                PopulateErrorCodesDropDownList(errorCodeSolution.ErrorCodeId);
                PopulateApplicationTypesDropDownList(errorCodeSolution.ApplicationTypeId);
                PopulateSoftwareApplicationsDropDownList(errorCodeSolution.SoftwareApplicationId);
            } catch (RetryLimitExceededException /* dex */) {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }

            return View(errorCodeSolution);
        }

        public ActionResult Delete(Guid? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ErrorCodeSolution errorCodeSolution = db.ErrorCodeSolutions.Find(id);
            if (errorCodeSolution == null) {
                return HttpNotFound();
            }
            return View(errorCodeSolution);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id) {
            ErrorCodeSolution errorCodeSolution = db.ErrorCodeSolutions.Find(id);
            db.ErrorCodeSolutions.Remove(errorCodeSolution);
            db.SaveChanges();
            return RedirectToAction("Edit", "Solution", new { id = errorCodeSolution.SolutionId });
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void PopulateSolutionsDropDownList(object selectedSolution = null) {
            ViewBag.SolutionID = new SelectList(db.Solutions.OrderBy(s => s.Description), "SolutionID", "Description", selectedSolution);
        }

        private void PopulateErrorCodesDropDownList(object selectedErrorCode = null) {
            ViewBag.ErrorCodeID = new SelectList(db.ErrorCodes.OrderBy(ec => ec.Code), "ErrorCodeID", "DisplayableName", selectedErrorCode);
        }

        private void PopulateApplicationTypesDropDownList(object selectedApplicationType = null) {
            List<ApplicationType> items = new List<ApplicationType>() { null };
            items.AddRange(db.ApplicationTypes.OrderBy(at => at.Name));
            ViewBag.ApplicationTypeID = new SelectList(items, "ApplicationTypeId", "Name", selectedApplicationType);
        }

        private void PopulateSoftwareApplicationsDropDownList(object selectedSoftwareApplication = null) {
            List<SoftwareApplication> items = new List<SoftwareApplication>() { null };
            items.AddRange(db.SoftwareApplications.OrderBy(sa => sa.Name));
            ViewBag.SoftwareApplicationID = new SelectList(items, "SoftwareApplicationID", "Name", selectedSoftwareApplication);
        }

        public static ErrorCodeSolutionResponse GetSolutionsForErrorCode(ErrorCodeContext db, string errorCode, string productType, string productCode) {
            ErrorCodeSolutionResponse solution = new ErrorCodeSolutionResponse() { SoftwareSpecificSolutionsForError = new List<string>(), ProductTypeSolutionsForError = new List<string>(), GenericSolutionsForError = new List<string>() };
            if (!string.IsNullOrWhiteSpace(errorCode)) {
                Guid errorCodeId = Guid.Empty;
                try {
                    ErrorCode ec = db.ErrorCodes.First(n => n.Code.ToLower() == errorCode.ToLower());
                    solution.ErrorCode = ec.Code;
                    solution.ErrorDescription = ec.Description;
                    errorCodeId = ec.ErrorCodeId;
                } catch (Exception) { // The error code is not found, so return an empty list 
                }
                if (!errorCodeId.Equals(Guid.Empty)) {
                    Guid applicationTypeId = Guid.Empty;
                    if (!string.IsNullOrWhiteSpace(productType)) {
                        foreach (var item in db.ApplicationTypes.Where(at => at.Name.ToLower() == productType.ToLower())) {
                            applicationTypeId = item.ApplicationTypeId;
                            break;
                        }
                    }

                    Guid softwareId = Guid.Empty;
                    if (!applicationTypeId.Equals(Guid.Empty) && !string.IsNullOrWhiteSpace(productCode)) {
                        foreach (var item in db.SoftwareApplications.Where(sa => sa.ApplicationTypeId == applicationTypeId && sa.ApplicationIdentifier.ToLower() == productCode.ToLower())) {
                            softwareId = item.SoftwareApplicationId;
                            break;
                        }
                    }

                    //Get the software specific items first
                    if (!softwareId.Equals(Guid.Empty)) {
                        foreach (var item in db.ErrorCodeSolutions
                                .Where(ecs => ecs.SoftwareApplicationId == softwareId && ecs.ErrorCodeId == errorCodeId)
                                .OrderBy(ecs => ecs.Solution.Description)
                                .Select(ecs => ecs.Solution.Description)) {
                            if (!solution.SoftwareSpecificSolutionsForError.Contains(item)) {
                                solution.SoftwareSpecificSolutionsForError.Add(item);
                            }
                        }
                    }

                    //Get the application type specific items first
                    if (!applicationTypeId.Equals(Guid.Empty)) {
                        foreach (var item in db.ErrorCodeSolutions
                                .Where(ecs => (ecs.SoftwareApplicationId == null || ecs.SoftwareApplicationId == Guid.Empty) && ecs.ApplicationTypeId == applicationTypeId && ecs.ErrorCodeId == errorCodeId)
                                .OrderBy(ecs => ecs.Solution.Description)
                                .Select(ecs => ecs.Solution.Description)) {
                            if (!solution.ProductTypeSolutionsForError.Contains(item)) {
                                solution.ProductTypeSolutionsForError.Add(item);
                            }
                        }
                    }

                    //Get any other solutions for this error last
                    foreach (var item in db.ErrorCodeSolutions
                            .Where(ecs => (ecs.SoftwareApplicationId == null || ecs.SoftwareApplicationId == Guid.Empty) && (ecs.ApplicationTypeId == null || ecs.ApplicationTypeId == Guid.Empty) && ecs.ErrorCodeId == errorCodeId)
                            .OrderBy(ecs => ecs.Solution.Description)
                            .Select(ecs => ecs.Solution.Description)) {
                        if (!solution.GenericSolutionsForError.Contains(item)) {
                            solution.GenericSolutionsForError.Add(item);
                        }
                    }
                }
            }

            return solution;
        }


    }
}
